// import React, {useState, Fragment} from "react";
// import LoginForm from './components/LoginForm';
// import data from "./mock-data.json";
// import { nanoid } from "nanoid";
// import ReadOnlyRow from "./components/ReadOnlyRow";
// import EditableRow from "./components/EditableRow";


// function App () {
//   const adminUser = {
//     email: "admin@admin.com",
//     password: "admin"
//   }

//   const [user, setUser] = useState({name: "", email: "",});
//   const [error, setError] = useState("");

//   const Login = details => {
//     console.log(details);
    
//     if (details.email == adminUser.email && details.password == adminUser.password){
//       console.log("Logged in");
//       setUser({
//         name: details.name,
//         email: details.email
//       });
//     } else {
//       console.log("Details do not match");
//       setError("Details do not match")
//     }
//   }

//   const Logout = () => {
//     setUser({name: "", email:""});

//   };

//   const [contacts, setContacts] = useState(data);
//   const [addFormData, setAddFormData] = useState({
//     firstName: "",
//     lastName: "",
//     email: "",
//     age: "",
//     password: "",
//     birthDate: ""
//   });

//   const [editFormData, setEditFormData] = useState({
//     firstName: "",
//     lastName: "",
//     email: "",
//     age: "",
//     password: "",
//     birthDate: ""
//   });

//   const [editContactId, setEditContactId] = useState(null);

//   const handleAddFormChange = (event) => {
//     event.preventDefault();

//     const fieldName = event.target.getAttribute("name");
//     const fieldValue = event.target.value;

//     const newFormData = { ...addFormData };
//     newFormData[fieldName] = fieldValue;

//     setAddFormData(newFormData);
//   };

//   const handleEditFormChange = (event) => {
//     event.preventDefault();

//     const fieldName = event.target.getAttribute("name");
//     const fieldValue = event.target.value;

//     const newFormData = { ...editFormData };
//     newFormData[fieldName] = fieldValue;

//     setEditFormData(newFormData);
//   };

//   const handleAddFormSubmit = (event) => {
//     event.preventDefault();

//     const newContact = {
//       id: nanoid(),
//       firstName: addFormData.firstName,
//       lastName: addFormData.lastName,
//       email:  addFormData.email,
//       age: addFormData.age,
//       password: addFormData.password,
//       birthDate: addFormData.birthDate
//     };

//     const newContacts = [...contacts, newContact];
//     setContacts(newContacts);
//   };

//   const handleEditFormSubmit = (event) => {
//     event.preventDefault();

//     const editedContact = {
//       id: editContactId,
//       firstName: editFormData.firstName, 
//       lastName: editFormData.lastName,
//       email: editFormData.email,
//       age: editFormData.age,
//       password: editFormData.password,
//       birthDate: editFormData.birthDate
//     };

//     const newContacts = [...contacts];

//     const index = contacts.findIndex((contact) => contact.id === editContactId);

//     newContacts[index] = editedContact;

//     setContacts(newContacts);
//     setEditContactId(null);
//   };

//   const handleEditClick = (event, contact) => {
//     event.preventDefault();
//     setEditContactId(contact.id);

//     const formValues = {
//       firstName: contact.firstName,
//       lastName: contact.lastName,
//       email: contact.email,
//       age: contact.age,
//       password: contact.password,
//       birthDate: contact.birthDate
//     };

//     setEditFormData(formValues);
//   };

//   const handleCancelClick = () => {
//     setEditContactId(null);
//   };

//   const handleDeleteClick = (contactId) => {
//     const newContacts = [...contacts];

//     const index = contacts.findIndex((contact) => contact.id === contactId);

//     newContacts.splice(index, 1);

//     setContacts(newContacts);
//   };

//   return (
//     <div className="App">
      
//       {(user.email != "") ? (
//         <div className="welcome">
//           <h2>Welcome, <span>{user.name}</span></h2>
//           <h2>Add a Contact</h2>
//           <form onSubmit={handleAddFormSubmit}>
//         <input
//           type="text"
//           name="firstName"
//           required="required"
//           placeholder="Enter firstname..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="lastName"
//           required="required"
//           placeholder="Enter lastname..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="email"
//           name="email"
//           required="required"
//           placeholder="Enter email..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="age"
//           required="required"
//           placeholder="Enter age..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="password"
//           required="required"
//           placeholder="Enter password..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="birthDate"
//           required="required"
//           placeholder="Enter birthdate..."
//           onChange={handleAddFormChange}
//         />
//         <button type="submit">Add</button>
//       </form>

//       <form onSubmit={handleEditFormSubmit}>
//         <table>
//           <thead>
//             <tr>
//               <th>First Name</th>
//               <th>Last Name</th>
//               <th>Email</th>
//               <th>Age</th>
//               <th>Password</th>
//               <th>Birthdate</th>
//             </tr>
//           </thead>
//           <tbody>
//             {contacts.map((contact) => (
//               <Fragment>
//                 {editContactId === contact.id ? (
//                   <EditableRow
//                     editFormData={editFormData}
//                     handleEditFormChange={handleEditFormChange}
//                     handleCancelClick={handleCancelClick}
//                   />
//                 ) : (
//                   <ReadOnlyRow
//                     contact={contact}
//                     handleEditClick={handleEditClick}
//                     handleDeleteClick={handleDeleteClick}
//                   />
//                 )}
//               </Fragment>
//             ))}
//           </tbody>
//         </table>
//       </form>
//           <button onClick={Logout}>Logout</button>
//         </div>
//       ) : (
//         <LoginForm Login={Login} error={error}/>
//       )}
//     </div>
//   )
// }

// export default App;

import { useState, useEffect, useContext } from 'react';
import { Form, Button, } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import {Link}  from 'react-router-dom';
import UserContext from '../UserContext'

export default function Login(props) {
    console.log(props);

    const {user, setUser} = useContext(UserContext)
    console.log(user)


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(true);

    console.log(email, password);
    // console.log(email);
    // console.log(password);


    function loginUser(e) {

        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.accessToken !== "undefined"){
              localStorage.setItem('token', data.accessToken)
              retrieveUserDetails(data.accessToken)

            }
        })

        setEmail('');
        setPassword('');

    }

    const retrieveUserDetails = (token) => {
      fetch('http://localhost:4000/users/getUserDetails', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        setUser({
          _id: data._id,
          isAdmin: data.isAdmin,
        })
      })
    }

useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

    return (

        (user.id !== null) ?
        <Navigate to= "/employeelist"/>
        :

        <>
        <h1>Login Here:</h1>
        <Form onSubmit={e => loginUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address: </Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label> Password: </Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

             { isActive ?
                <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Login
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
                    Login
                </Button>
            }
        </Form>
        </>

    )

}
