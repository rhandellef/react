import {useEffect, useState, useContext} from 'react';
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import {useParams, Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function CourseView(){

  const {user} = useContext(UserContext);


  // retrieve courseId from URL
  const {courseId} = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);

  const enroll = (courseId) => {

    fetch('http://localhost:4000/getAllEmployees', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        courseId: courseId
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
  }

  useEffect(() => {
    console.log(courseId);

    fetch(`http://localhost:4000/getAllUsers`)
    .then(res => res.json())
    .then(data => {
      console.log(data)

      setName(data.name)
      setPrice(data.price)
      setDescription(data.description)

    })

  }, [courseId])

  return (

    <Container className="mt-5">
          <Col lg={{span: 6, offset: 3}}>
            <table>
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Age</th>
                  <th>Password</th>
                  <th>Birthdate</th>
                </tr>
              </thead>
            </table>
          </Col>
    </Container>
  )
}