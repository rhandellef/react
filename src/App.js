import {useEffect, useState} from 'react'
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Login from './pages/Login';
import EmployeeList from './pages/EmployeeList';
import './App.css';
import { UserProvider } from './UserContext';

function App () {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unSetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    fetch('https://localhost:4000/getAllUsers', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
        <Container>
          <Routes>
          <Route exact path="/" element={<Login/>}/>
          <Route exact path="/employeelist" element={<EmployeeList/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  )
}


export default App;



































// import React, {useState, Fragment} from "react";
// import LoginForm from './components/LoginForm';
// import data from "./mock-data.json";
// import { nanoid } from "nanoid";
// import ReadOnlyRow from "./components/ReadOnlyRow";
// import EditableRow from "./components/EditableRow";


// function App () {
//   const adminUser = {
//     email: "admin@admin.com",
//     password: "admin"
//   }

//   const [user, setUser] = useState({name: "", email: "",});
//   const [error, setError] = useState("");

//   const Login = details => {
//     console.log(details);
    
//     if (details.email == adminUser.email && details.password == adminUser.password){
//       console.log("Logged in");
//       setUser({
//         name: details.name,
//         email: details.email
//       });
//     } else {
//       console.log("Details do not match");
//       setError("Details do not match")
//     }
//   }

//   const Logout = () => {
//     setUser({name: "", email:""});

//   };

//   const [contacts, setContacts] = useState(data);
//   const [addFormData, setAddFormData] = useState({
//     firstName: "",
//     lastName: "",
//     email: "",
//     age: "",
//     password: "",
//     birthDate: ""
//   });

//   const [editFormData, setEditFormData] = useState({
//     firstName: "",
//     lastName: "",
//     email: "",
//     age: "",
//     password: "",
//     birthDate: ""
//   });

//   const [editContactId, setEditContactId] = useState(null);

//   const handleAddFormChange = (event) => {
//     event.preventDefault();

//     const fieldName = event.target.getAttribute("name");
//     const fieldValue = event.target.value;

//     const newFormData = { ...addFormData };
//     newFormData[fieldName] = fieldValue;

//     setAddFormData(newFormData);
//   };

//   const handleEditFormChange = (event) => {
//     event.preventDefault();

//     const fieldName = event.target.getAttribute("name");
//     const fieldValue = event.target.value;

//     const newFormData = { ...editFormData };
//     newFormData[fieldName] = fieldValue;

//     setEditFormData(newFormData);
//   };

//   const handleAddFormSubmit = (event) => {
//     event.preventDefault();

//     const newContact = {
//       id: nanoid(),
//       firstName: addFormData.firstName,
//       lastName: addFormData.lastName,
//       email:  addFormData.email,
//       age: addFormData.age,
//       password: addFormData.password,
//       birthDate: addFormData.birthDate
//     };

//     const newContacts = [...contacts, newContact];
//     setContacts(newContacts);
//   };

//   const handleEditFormSubmit = (event) => {
//     event.preventDefault();

//     const editedContact = {
//       id: editContactId,
//       firstName: editFormData.firstName, 
//       lastName: editFormData.lastName,
//       email: editFormData.email,
//       age: editFormData.age,
//       password: editFormData.password,
//       birthDate: editFormData.birthDate
//     };

//     const newContacts = [...contacts];

//     const index = contacts.findIndex((contact) => contact.id === editContactId);

//     newContacts[index] = editedContact;

//     setContacts(newContacts);
//     setEditContactId(null);
//   };

//   const handleEditClick = (event, contact) => {
//     event.preventDefault();
//     setEditContactId(contact.id);

//     const formValues = {
//       firstName: contact.firstName,
//       lastName: contact.lastName,
//       email: contact.email,
//       age: contact.age,
//       password: contact.password,
//       birthDate: contact.birthDate
//     };

//     setEditFormData(formValues);
//   };

//   const handleCancelClick = () => {
//     setEditContactId(null);
//   };

//   const handleDeleteClick = (contactId) => {
//     const newContacts = [...contacts];

//     const index = contacts.findIndex((contact) => contact.id === contactId);

//     newContacts.splice(index, 1);

//     setContacts(newContacts);
//   };

//   return (
//     <div className="App">
      
//       {(user.email != "") ? (
//         <div className="welcome">
//           <h2>Welcome, <span>{user.name}</span></h2>
//           <h2>Add a Contact</h2>
//           <form onSubmit={handleAddFormSubmit}>
//         <input
//           type="text"
//           name="firstName"
//           required="required"
//           placeholder="Enter firstname..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="lastName"
//           required="required"
//           placeholder="Enter lastname..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="email"
//           name="email"
//           required="required"
//           placeholder="Enter email..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="age"
//           required="required"
//           placeholder="Enter age..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="password"
//           required="required"
//           placeholder="Enter password..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="birthDate"
//           required="required"
//           placeholder="Enter birthdate..."
//           onChange={handleAddFormChange}
//         />
//         <button type="submit">Add</button>
//       </form>

//       <form onSubmit={handleEditFormSubmit}>
//         <table>
//           <thead>
//             <tr>
//               <th>First Name</th>
//               <th>Last Name</th>
//               <th>Email</th>
//               <th>Age</th>
//               <th>Password</th>
//               <th>Birthdate</th>
//             </tr>
//           </thead>
//           <tbody>
//             {contacts.map((contact) => (
//               <Fragment>
//                 {editContactId === contact.id ? (
//                   <EditableRow
//                     editFormData={editFormData}
//                     handleEditFormChange={handleEditFormChange}
//                     handleCancelClick={handleCancelClick}
//                   />
//                 ) : (
//                   <ReadOnlyRow
//                     contact={contact}
//                     handleEditClick={handleEditClick}
//                     handleDeleteClick={handleDeleteClick}
//                   />
//                 )}
//               </Fragment>
//             ))}
//           </tbody>
//         </table>
//       </form>
//           <button onClick={Logout}>Logout</button>
//         </div>
//       ) : (
//         <LoginForm Login={Login} error={error}/>
//       )}
//     </div>
//   )
// }

// export default App;