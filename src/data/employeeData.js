const employeeData = [
    {
        id: "1",
        firstName: "Captain",
        lastName: "America",
        email: "camerica@gmail.com",
        age: "22",
        password: "user123",
        birthDate: "August 1, 2022"
    },
    {
        id: "2",
        firstName: "Iron",
        lastName: "Man",
        email: "iman@gmail.com",
        age: "22",
        password: "user123",
        birthDate: "August 1, 2022"
    },
    {
        id: "3",
        firstName: "Thor",
        lastName: "Hammer",
        email: "thammer@gmail.com",
        age: "22",
        password: "user123",
        birthDate: "August 1, 2022"
    }
  ]
  
  export default employeeData;